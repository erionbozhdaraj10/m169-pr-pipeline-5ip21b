# Menü

Menü | Mo | Di | Mi | Do | Fr |
|--|--|--|--|--|--|
| *Global* | Gebratenes Steak vom Blumenkohl  Pfeffersauce  Süsskartoffelfries | Planted. Geschnetzeltes  Pommery Senfsauce  Röstikroketten  Erbsen | Kräuter-Gnocchis  Gemüse-Tomaten Pesto  Sommerliches Grillgemüse | Halloumi-Zucchetti Bratling  Gemüsepfanne mit Kichererbsen  Joghurt-Harissadip  Granatapfelkerne | «Pasta Basta»  Bedienen sie sich an verschiedenen Ravioli, Pasta, Saucen und Pesto  Fleisch: Schweiz / Fisch: Norwegen |
|--|--|--|--|--|--|
| *Traditional* | «Pollo fino»  Tomatensauce mit Oliven und Basilikum  Safranrisotto  Romanesco    Fleisch: Schweiz | Spaghetti  «a la Carbonara»  mit Speck und Rahm    Fleisch: Schweiz | Paniertes Schweinsschnitzel  Pommes frites  Buntes Gemüse    Fleisch: Schweiz | Rindshackbraten  Rotweinsauce  Hausgemachte Spätzli  Bohnen    Fleisch: Schweiz |Schlemmerfilet  «a la Bordelaise»  Petersilienkartoffeln  Rahmspinat    Fisch: Nordostatlantik |
